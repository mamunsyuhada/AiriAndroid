package id.co.mertani.airi.airi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChartActivity extends AppCompatActivity {

    private DatabaseReference rootRef;
    private DatabaseReference demoRef;
    private ArrayList lumens;
    private LineChart chart;
    private Entry lumen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        setupActionBar();
        rootRef = FirebaseDatabase.getInstance().getReference();
        chart = findViewById(R.id.chart_line);
        demoRef = rootRef.child("Airi").child("1").child("db");
        lumens = new ArrayList();
        demoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()){
                    for (DataSnapshot data : dataSnapshot.getChildren()){
                        lumen = new Entry();
                        Log.d("timestamp", data.child("timestamp").getValue().toString().substring(14, 16));
                        lumen.setX(Float.parseFloat(data.child("timestamp").getValue().toString().substring(14, 16)));
                        lumen.setY(Float.parseFloat(data.child("lumen").getValue().toString()));
                        lumens.add(lumen);

                        LineDataSet lumenDataSet = new LineDataSet(lumens, "Lumen");
                        lumenDataSet.setColor(getResources().getColor(R.color.colorAccent));
                        lumenDataSet.setValueTextColor(getResources().getColor(R.color.colorPrimary));

                        LineData lumenData = new LineData(lumenDataSet);
                        chart.setData(lumenData);
                        chart.invalidate();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.home){
            Intent add = new Intent(ChartActivity.this, AddActivity.class);
            startActivity(add);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

package id.co.mertani.airi.airi;

/**
 * Created by Mertani on 12/12/2017.
 */

public class Models {
    private String lumen;
    private String moisture;
    private String nutrition;
    private String rh;
    private String temp;
    private String timestamp;

    public String getLumen() {
        return lumen;
    }

    public void setLumen(String lumen) {
        this.lumen = lumen;
    }

    public String getMoisture() {
        return moisture;
    }

    public void setMoisture(String moisture) {
        this.moisture = moisture;
    }

    public String getNutrition() {
        return nutrition;
    }

    public void setNutrition(String nutrition) {
        this.nutrition = nutrition;
    }

    public String getRh() {
        return rh;
    }

    public void setRh(String rh) {
        this.rh = rh;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
